#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import ConfigParser
from hermes_python.hermes import Hermes
from hermes_python.ontology import *
# import sys
# sys.path.append('/usr/local/lib/python2.7/dist-packages')
# from gpiozero import LED
import RPi.GPIO as GPIO
import io
import time
import serial

CONFIGURATION_ENCODING_FORMAT = "utf-8"
# CONFIG_INI = "config.ini"

ser = serial.Serial(
              
               port='/dev/serial0',
               baudrate = 9600,
               parity=serial.PARITY_NONE,
               stopbits=serial.STOPBITS_ONE,
               bytesize=serial.EIGHTBITS,
               timeout=1
           )

# led = LED(17)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.output(17,GPIO.LOW)
GPIO.output(27,GPIO.LOW)
# you have to add _snips and _snips-skills to the group gpio
# sudo adduser _snips gpio
# sudo adduser _snips-skills gpio
# otherwise it does not work for me

# class SnipsConfigParser(ConfigParser.SafeConfigParser):
#   def to_dict(self):
#       return {section : {option_name : option for option_name, option in self.items(section)} for section in self.sections()}

# def read_configuration_file(configuration_file):
#     try:
#         with io.open(configuration_file, encoding=CONFIGURATION_ENCODING_FORMAT) as f:
#             conf_parser = SnipsConfigParser()
#             conf_parser.readfp(f)
#             return conf_parser.to_dict()
#     except (IOError, ConfigParser.Error) as e:
#         return dict()

def subscribe_intent_callback(hermes, intentMessage):
    # conf = read_configuration_file(CONFIG_INI)
    # action_wrapper(hermes, intentMessage, conf)
    intentname = intentMessage.intent.intent_name
    if intentname == "RaspiSnips:LichtAn":
        result_sentence = "Das Licht ist eingeschaltet"
        # led.on()
        GPIO.output(17,GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(17,GPIO.LOW)
        hermes.publish_end_session(intentMessage.session_id, result_sentence)

    elif intentname == "RaspiSnips:LichtAus":
        result_sentence = "Das Licht ist ausgeschaltet"
        # led.off()
        GPIO.output(27,GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(27,GPIO.LOW)
        hermes.publish_end_session(intentMessage.session_id, result_sentence)

    elif intentname == "RaspiSnips:MusikAn":
        result_sentence = "Ich habe die Musik eingeschaltet"
        ser.write('MUSIC_ON')
        hermes.publish_end_session(intentMessage.session_id, result_sentence)

    elif intentname == "RaspiSnips:MusikAus":
        result_sentence = "Ich habe die Musik ausgeschaltet"
        ser.write('MUSIC_OFF')
        hermes.publish_end_session(intentMessage.session_id, result_sentence)


#def action_wrapper(hermes, intentMessage, conf):
#    """ Write the body of the function that will be executed once the intent is recognized. 
#    In your scope, you have the following objects : 
#    - intentMessage : an object that represents the recognized intent
#    - hermes : an object with methods to communicate with the MQTT bus following the hermes protocol. 
#    - conf : a dictionary that holds the skills parameters you defined 
#    Refer to the documentation for further details. 
#    """ 
#    led.on()
#    result_sentence = "Die LED ist eingeschaltet"
#    current_session_id = intentMessage.session_id
#    hermes.publish_end_session(current_session_id, result_sentence)



if __name__ == "__main__":
    with Hermes("localhost:1883") as h:
        h.subscribe_intents(subscribe_intent_callback).start()
#h.subscribe_intent("bertron:GPIOhigh", subscribe_intent_callback).start()
